#application of a raw string
#its rarely used and used in regular expressions, related to git
#it ignores the backslash character \n for example, but it does not ignore \' or \"
print('Hello \n world')
print(r'Hello \n \" world\"')
print('Hello \n \'python\' ')
print(r'Hello \n \'python\'')
