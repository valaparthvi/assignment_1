#Performing MIN(), MAX(), and SUM() operations 

list1 = [1,-2,4,-5.6,7.6,32]
print(list1)

print("Maximum number in list1 is ",max(list1))
print("Minimum number in list1 is ",min(list1))
print("Sum of all the values in list1 is ",sum(list1))
