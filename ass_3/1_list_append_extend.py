#performing append and extend operations on lists 

l_1 = [1,2,3,'parthvi',4.5,'63']

l_2 = [45,'c',4.78,1,14,'theory']

print("List 1 is:\n",l_1)

print("List 2 is:\n",l_2)

print("Appending list 2 to list 1 gives:")
l_1.append(l_2)
print(l_1)

print("Extending list 2 to list 1 gives:")
l_1.extend(l_2)
print(l_1)
