#a03p02a
l1 = [a for a in range(1,11)]
print("l1 : ")
print(l1)

#​a03p02b
l2 = [b for b in range(10,110,10)]
print("\n l2 : ")
print(l2)

#​a03p02c
l3 = ['python','django','flask','string','function','classes']
print("\n l3 : ")
print(l3)

#​a03p02d

l4 = {'l1':'list l1', 'l2':'list l2', 'l3': 'list l3'}
print("\n l4 : ")
print(l4)

#a03p02e

main_list_1 = []
main_list_1.append(l1)
main_list_1.append(l2)
main_list_1.append(l3)

print("\n A main list with list 1,2 and 3 appended together is:")
print(main_list_1)

main_list_2 = []
main_list_2.extend(l1)
main_list_2.extend(l2)
main_list_2.extend(l3)
print("\n A main list with list 1,2 and 3 extended together is:")
print(main_list_2)

#a03p02f

l5 = l1*2
print("\n l5 : ")
print(l5)

#a03p02g

main_list_1.append(l5)
print("\n After appending list 5 to the main list: ")
print(main_list_1)

#a03p02h

p =main_list_1.count(1)
print("\n Occurences of integer 1 in extended main list is: ",p)

q = main_list_2.count(1)
print("\n Occurences of integer 1 in appended main list is: ",q )
