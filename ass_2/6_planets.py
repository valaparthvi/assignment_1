pl_nm = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune']

print(pl_nm)

inp_pl = input("Please enter a planets name  ")

if inp_pl in pl_nm[0:2]:
    print("You're in the inner planet.")

elif inp_pl == pl_nm[2]:
    print("You're on Planet Earth.")

else:
    print("You're in the outer planet")
