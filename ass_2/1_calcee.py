con_ = True
while con_ == True:
    num_1 = int(input("Please enter number_1 : "))
    num_2 = int(input("Please enter number_2 : "))
    op = input("""Please enter
    '+' for addition
    '-' for substraction
    '*' for multiplication
    '/' for division
    """)

    if op == '+':
        print("%d + %d = "%(num_1,num_2),num_1 + num_2)
    elif op == '-':
        print("%d - %d = "%(num_1,num_2),num_1 - num_2)
    elif op == '*':
        print("%d * %d = "%(num_1,num_2),num_1 * num_2)
    elif op == '/':
        if num_2 != 0:
            print("%d / %d = "%(num_1,num_2),num_1 / num_2)
        else:
            print("Please enter a non-zero denominator and try again!")
    else:
        print("Please enter a valid character ")

    ans = input("Do you wish to continue? Y or N? ")
    if ans == 'Y':
        con_ = True
    else:
        con_ = False
